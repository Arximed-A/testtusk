const srcArr = [
  "Муза, ранясь шилом опыта, ты помолишься на разума", //если бы не последння буква, то был бы палиндром
  "шалаш",
  false,
  12342321,
  "потоп",
  8282882892,
  [],
  [1, 2, 2, 2, 3],
  true,
  123433334321,
  "манекенам",
  "водоворот",
  "Он рёва наверно",
];

const palindrom = findPalindrom(srcArr);

console.log(palindrom);
// по условию палиндромом являются только слова и числа. Остальное отсекаем
function isPalindrome(str) {
  const cleanedStr = str
    .replace(/[\s.,%]/g, "")
    .replace(/ё/gi, "е")
    .toLowerCase();
  const reversedStr = cleanedStr.split("").reverse().join("");
  return cleanedStr === reversedStr;
}

function findPalindrom(arr) {
  const palindromArray = [];
  arr.forEach((el) => {
    if (typeof el === "string" || typeof el === "number") {
      const stringRepresentation = el.toString();
      if (isPalindrome(stringRepresentation)) palindromArray.push(el);
    }
  });

  return palindromArray.length !== 0 ? palindromArray : null;
}
